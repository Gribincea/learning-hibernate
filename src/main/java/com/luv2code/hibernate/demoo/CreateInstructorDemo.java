package com.luv2code.hibernate.demoo;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class CreateInstructorDemo {

    public static void main(String[] args) {

        // create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try {
            Instructor instructor = new Instructor("Valentin", "Ivanov","valivangmail.com");
            InstructorDetail instructorDetail = new InstructorDetail("http://luv182ode.com/youtube", "reading");
            instructor.setInstructorDetail(instructorDetail);

            session.beginTransaction();
            System.out.println("Saving instructor" + instructor);
            session.save(instructor);
            session.getTransaction().commit();


        } finally {
            session.close();
            factory.close();
        }
    }

}





