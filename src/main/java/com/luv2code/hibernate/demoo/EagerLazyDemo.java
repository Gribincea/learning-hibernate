package com.luv2code.hibernate.demoo;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;


public class EagerLazyDemo {

    public static void main(String[] args) {

        // create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try {
            session.beginTransaction();
            int theId = 2;

           Query<Instructor> query= session.createQuery("select i from Instructor i join fetch i.courses where i.id=:theInstructorId" , Instructor.class);
           query.setParameter("theInstructorId", theId);
           Instructor tempInstructor=query.getSingleResult();


            session.getTransaction().commit();
            session.close();
            System.out.println("temp instructor " + tempInstructor.getCourses());
            System.out.println("luv2code done");
        } finally {
            session.close();
            factory.close();
        }
    }

}





