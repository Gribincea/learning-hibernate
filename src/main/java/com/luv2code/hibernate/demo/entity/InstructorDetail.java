package com.luv2code.hibernate.demo.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
//@SequenceGenerator(name = "instructor_seqq", initialValue = 3, allocationSize = 1, sequenceName = "instructor_seq")

@Table(name = "INSTRUCTOR_DETAIL")
public class InstructorDetail {

    @Id
    @GenericGenerator(name = "gen", strategy = "increment")
    @GeneratedValue(generator = "gen")
//   / @GeneratedValue(strategy = SEQUENCE,generator = "instructor_seqq")
    @Column(name = "ID")
    private int id;
    @OneToOne(mappedBy = "instructorDetail", cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private Instructor instructor;

    public Instructor getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    @Column(name = "YOUTUBE_CHANEL")
    private String youtubeChan;

    public InstructorDetail() {
    }

    public InstructorDetail(String youtubeChan, String hobby) {
        this.youtubeChan = youtubeChan;
        this.hobby = hobby;
    }

    @Override
    public String toString() {
        return "InstructorDetail{" +
                "id=" + id +
                ", youtubeChan='" + youtubeChan + '\'' +
                ", hobby='" + hobby + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getYoutubeChan() {
        return youtubeChan;
    }

    public void setYoutubeChan(String youtubeChan) {
        this.youtubeChan = youtubeChan;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    @Column(name = "HOBBY")
    private String hobby;

}
