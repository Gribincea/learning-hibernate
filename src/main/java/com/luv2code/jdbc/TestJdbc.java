package com.luv2code.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class TestJdbc {
    public static void main(String[] args) {
        String jdbcUrl="jdbc:oracle:thin:@//localhost:1521/orclpdb";
        String user="hr";
        String pass="hr";
        try {
            Connection connection= DriverManager.getConnection(jdbcUrl,user,pass);
            System.out.println(connection.isClosed());
        }
        catch (Exception ex){
            ex.printStackTrace();

        }
    }
}
